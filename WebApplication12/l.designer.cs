﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication12
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="master")]
	public partial class lDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region 可扩展性方法定义
    partial void OnCreated();
    partial void InsertT(T instance);
    partial void UpdateT(T instance);
    partial void DeleteT(T instance);
    partial void InsertTable(Table instance);
    partial void UpdateTable(Table instance);
    partial void DeleteTable(Table instance);
    #endregion
		
		public lDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["masterConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public lDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public lDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public lDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public lDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<T> T
		{
			get
			{
				return this.GetTable<T>();
			}
		}
		
		public System.Data.Linq.Table<Table> Table
		{
			get
			{
				return this.GetTable<Table>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.T")]
	public partial class T : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private string _Name;
		
		private int _idnumber;
		
		private string _temp;
		
    #region 可扩展性方法定义
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnNameChanging(string value);
    partial void OnNameChanged();
    partial void OnidnumberChanging(int value);
    partial void OnidnumberChanged();
    partial void OntempChanging(string value);
    partial void OntempChanged();
    #endregion
		
		public T()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Name", DbType="NChar(10)")]
		public string Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				if ((this._Name != value))
				{
					this.OnNameChanging(value);
					this.SendPropertyChanging();
					this._Name = value;
					this.SendPropertyChanged("Name");
					this.OnNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_idnumber", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int idnumber
		{
			get
			{
				return this._idnumber;
			}
			set
			{
				if ((this._idnumber != value))
				{
					this.OnidnumberChanging(value);
					this.SendPropertyChanging();
					this._idnumber = value;
					this.SendPropertyChanged("idnumber");
					this.OnidnumberChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_temp", DbType="NChar(10)")]
		public string temp
		{
			get
			{
				return this._temp;
			}
			set
			{
				if ((this._temp != value))
				{
					this.OntempChanging(value);
					this.SendPropertyChanging();
					this._temp = value;
					this.SendPropertyChanged("temp");
					this.OntempChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.[Table]")]
	public partial class Table : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private string _学号;
		
		private string _今日体温;
		
		private string _是否有头晕发热等不适症状;
		
		private string _近14天是否去过中高风险地区;
		
		private string _家人是否有头晕发热等不适症状;
		
    #region 可扩展性方法定义
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void On学号Changing(string value);
    partial void On学号Changed();
    partial void On今日体温Changing(string value);
    partial void On今日体温Changed();
    partial void On是否有头晕发热等不适症状Changing(string value);
    partial void On是否有头晕发热等不适症状Changed();
    partial void On近14天是否去过中高风险地区Changing(string value);
    partial void On近14天是否去过中高风险地区Changed();
    partial void On家人是否有头晕发热等不适症状Changing(string value);
    partial void On家人是否有头晕发热等不适症状Changed();
    #endregion
		
		public Table()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_学号", DbType="NChar(10) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
		public string 学号
		{
			get
			{
				return this._学号;
			}
			set
			{
				if ((this._学号 != value))
				{
					this.On学号Changing(value);
					this.SendPropertyChanging();
					this._学号 = value;
					this.SendPropertyChanged("学号");
					this.On学号Changed();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_今日体温", DbType="NChar(10)")]
		public string 今日体温
		{
			get
			{
				return this._今日体温;
			}
			set
			{
				if ((this._今日体温 != value))
				{
					this.On今日体温Changing(value);
					this.SendPropertyChanging();
					this._今日体温 = value;
					this.SendPropertyChanged("今日体温");
					this.On今日体温Changed();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_是否有头晕发热等不适症状", DbType="NChar(10)")]
		public string 是否有头晕发热等不适症状
		{
			get
			{
				return this._是否有头晕发热等不适症状;
			}
			set
			{
				if ((this._是否有头晕发热等不适症状 != value))
				{
					this.On是否有头晕发热等不适症状Changing(value);
					this.SendPropertyChanging();
					this._是否有头晕发热等不适症状 = value;
					this.SendPropertyChanged("是否有头晕发热等不适症状");
					this.On是否有头晕发热等不适症状Changed();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_近14天是否去过中高风险地区", DbType="NChar(10)")]
		public string 近14天是否去过中高风险地区
		{
			get
			{
				return this._近14天是否去过中高风险地区;
			}
			set
			{
				if ((this._近14天是否去过中高风险地区 != value))
				{
					this.On近14天是否去过中高风险地区Changing(value);
					this.SendPropertyChanging();
					this._近14天是否去过中高风险地区 = value;
					this.SendPropertyChanged("近14天是否去过中高风险地区");
					this.On近14天是否去过中高风险地区Changed();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_家人是否有头晕发热等不适症状", DbType="NChar(10)")]
		public string 家人是否有头晕发热等不适症状
		{
			get
			{
				return this._家人是否有头晕发热等不适症状;
			}
			set
			{
				if ((this._家人是否有头晕发热等不适症状 != value))
				{
					this.On家人是否有头晕发热等不适症状Changing(value);
					this.SendPropertyChanging();
					this._家人是否有头晕发热等不适症状 = value;
					this.SendPropertyChanged("家人是否有头晕发热等不适症状");
					this.On家人是否有头晕发热等不适症状Changed();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
