﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="管理.aspx.cs" Inherits="WebApplication12.管理" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        
        <div>
            <asp:Label ID="Label1" runat="server" Text="欢迎进入管理界面" ForeColor="#CC0066"></asp:Label>
        </div>
        <asp:GridView ID="GridView1" runat="server"  BackColor="White" BorderColor="#CC9966" BorderStyle="Double" BorderWidth="1px" CellPadding="4"  AutoGenerateColumns="False" DataKeyNames="idnumber"   OnRowDataBound="GridView1_RowDataBound" OnRowDeleting="GridView1_RowDeleting" EnablePersistedSelection="True" EnableSortingAndPagingCallbacks="True" ViewStateMode="Enabled"   >
            <Columns>
                <asp:BoundField DataField="idnumber" HeaderText="idnumber" SortExpression="idnumber" />
                <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="True" SortExpression="Name" InsertVisible="true" />
                <asp:BoundField DataField="temp" HeaderText="temp" SortExpression="temp" />
                
                <asp:HyperLinkField DataNavigateUrlFields="idnumber" DataNavigateUrlFormatString="xiugai.aspx" Text="编辑" HeaderText="编辑" />
                <asp:CommandField HeaderText="删除" ShowDeleteButton="True" ShowHeader="True" />
            </Columns>
            <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
            <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
            <RowStyle BackColor="White" ForeColor="#330099" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
            <SortedAscendingCellStyle BackColor="#FEFCEB" />
            <SortedAscendingHeaderStyle BackColor="#AF0101" />
            <SortedDescendingCellStyle BackColor="#F6F0C0" />
            <SortedDescendingHeaderStyle BackColor="#7E0000" />
        </asp:GridView>
        

      
    </form>
</body>
</html>
