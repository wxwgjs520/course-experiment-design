﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="提交信息.aspx.cs" Inherits="WebApplication12.提交信息" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="已成功提交信息"></asp:Label>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="学号">
                <Columns>
                    <asp:BoundField DataField="学号" HeaderText="学号" ReadOnly="True" SortExpression="学号" />
                    <asp:BoundField DataField="今日体温" HeaderText="今日体温" SortExpression="今日体温" />
                    <asp:BoundField DataField="是否有头晕发热等不适症状" HeaderText="是否有头晕发热等不适症状" SortExpression="是否有头晕发热等不适症状" />
                    <asp:BoundField DataField="近14天是否去过中高风险地区" HeaderText="近14天是否去过中高风险地区" SortExpression="近14天是否去过中高风险地区" />
                    <asp:BoundField DataField="家人是否有头晕发热等不适症状" HeaderText="家人是否有头晕发热等不适症状" SortExpression="家人是否有头晕发热等不适症状" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:masterConnectionString %>" SelectCommand="SELECT * FROM [Table]"></asp:SqlDataSource>
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="返回" />
        </div>
    </form>
</body>
</html>
