﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication12
{
    public partial class student : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label2.Text = Request.QueryString + "你好！欢迎登陆健康监测系统";
            Session["xuehao"] = TextBox2.Text;
            Session["tiwen"] = TextBox1.Text;
            if (RadioButton1.Checked == true)
            { Session["touyun"] = "yes"; }
            else { Session["touyun"] = "no"; }
            if (RadioButton3.Checked == true)
            { Session["gaofeng"] = "yes"; }
            else { Session["gaofeng"] = "no"; }
            if (RadioButton5.Checked == true)
            { Session["fare"] = "yes"; }
            else { Session["fare"] = "no"; }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Write("<script>alert('已提交成功！')</script>");
            Response.Redirect("提交信息.aspx");
            
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("疫情防控小知识.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("denglu.aspx");
        }
    }
    }
