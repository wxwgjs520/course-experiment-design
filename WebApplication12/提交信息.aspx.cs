﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication12
{
    public partial class 提交信息 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) { 
            lDataContext db = new lDataContext();
           
            Table wxw = new Table();
            wxw.学号 = (string)Session["xuehao"];
            wxw.今日体温 = (string)Session["tiwen"];
            wxw.是否有头晕发热等不适症状 = (string)Session["touyun"];
            wxw.家人是否有头晕发热等不适症状 = (string)Session["fare"];
            wxw.近14天是否去过中高风险地区 = (string)Session["gaofeng"];
            db.Table.InsertOnSubmit(wxw);
            db.SubmitChanges();
            bind();
}
        }
        protected void bind() 
        {
            lDataContext db = new lDataContext();

            var results =from r in db.Table
                         select r;
            GridView1.DataSource = results;
            GridView1.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("student.aspx");
        }
    }
}